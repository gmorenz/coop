pub mod asm;

use std::mem;
use std::ptr;

#[repr(C)]
pub struct StackPtr(usize);
pub const NULL_STACK_PTR: StackPtr = StackPtr(0);

pub unsafe trait StackData {
    /// Returns a buffer to use as a stack. The stack must be 16 byte alligned
    /// offset by 8 bytes, i.e. (addr + 8) % 16 == 0. It must be large enough
    /// that the program doesn't overflow. Only addresses before the address
    /// pointed to are read/written, so it is ok if that points one past the end
    /// of the allocation.
    fn start_ptr(&mut self) -> *mut u8;
}

unsafe impl <'a, T: StackData> StackData for &'a mut T {
    fn start_ptr(&mut self) -> *mut u8 {
        T::start_ptr(self)
    }
}

struct StartArgs<F: FnOnce(S, StackPtr) -> !, S: StackData> {
    func: F,
    stack: S
}

pub fn start<F, S: StackData>(mut stack: S, func: F)
where F: FnOnce(S, StackPtr) -> ! {

    extern "C" fn f_wrapper<F, S>(parent: StackPtr, args: *mut ()) -> !
    where F: FnOnce(S, StackPtr) -> !, S: StackData {
        let StartArgs{func, stack} = unsafe{ ptr::read(args as *mut StartArgs<F, S>) };
        func(stack, parent)
    }

    let data = stack.start_ptr();
    let mut args = StartArgs{ func, stack };

    unsafe {
        asm::start(data, f_wrapper::<F, S>, &mut args as *mut StartArgs<F, S> as *mut ());
    }
    mem::forget(args);
}

pub fn exit_to(target: StackPtr) -> ! {
    unsafe{ self::asm::exit_to(target) }
}

pub use self::asm::switch as unsafe_switch;
pub fn switch(target: StackPtr, save: &mut StackPtr) {
    unsafe{ unsafe_switch(target, save) }
}