use super::StackPtr;

// Memory layout used in saving registers (top to bottom, formated as "register (size)")
// | rbx (8) | rbp (8) | r12 (8) | r13 (8) | r14 (8) | r15 (8) | MXCSR (4) | x87 control (2) | free space for alignment (10) |

#[allow(improper_ctypes)]
extern "C" {
    /// Start a new cothread, buf *must* be 16 byte aligned, points to the end of a buffer to use as a stack.
    /// to_call is called with the stack pointer to the parent, and the pointer passed to start.
    pub fn start(buf: *mut u8, to_call: unsafe extern "C" fn(StackPtr, *mut()) -> !, args: *mut ());
}

global_asm! {r"
    .global start
start:
        # Save callee save registers
    sub     $64, %rsp
    mov     %rbx, (%rsp)
    mov     %rbp, 8(%rsp)
    mov     %r12, 16(%rsp)
    mov     %r13, 24(%rsp)
    mov     %r14, 32(%rsp)
    mov     %r15, 40(%rsp)
    stmxcsr 48(%rsp)
    fnstcw  52(%rsp)

    mov      %rsp, %rax         # Store original stackpointer, to pass to to_call.
    mov      %rdi, %rsp         # move stack pointer to new stack
    mov      %rax, %rdi         # arg1 is the stack pointer
    mov      %rsi, %rax         # save to_call
    mov      %rdx, %rsi         # arg2 is the passed in argument
    jmp     *%rax              # call to_call
    # Returning from this function is dealt with in `exit_to`/`switch`.
"}

extern "C" {
    /// Passes control from the current cothread to stack, with no way of returning.
    pub fn exit_to(stack: StackPtr) -> !;
}

global_asm! {r"
    .global exit_to
exit_to:
    mov     %rdi, %rsp          # Switch stack pointer
        # Restore callee save registers
    mov     (%rsp), %rbx
    mov     8(%rsp), %rbp
    mov     16(%rsp), %r12
    mov     24(%rsp), %r13
    mov     32(%rsp), %r14
    mov     40(%rsp), %r15
    ldmxcsr 48(%rsp)
    fldcw   52(%rsp)
    add     $64, %rsp           # Restore stack and return
    ret
"}

extern "C" {
    pub fn switch(target: StackPtr, save: *mut StackPtr);
}

global_asm! {r"
    .global switch
switch:
        # Save callee save registers
    sub     $64, %rsp
    mov     %rbx, (%rsp)
    mov     %rbp, 8(%rsp)
    mov     %r12, 16(%rsp)
    mov     %r13, 24(%rsp)
    mov     %r14, 32(%rsp)
    mov     %r15, 40(%rsp)
    stmxcsr 48(%rsp)
    fnstcw  52(%rsp)

    mov     %rsp, (%rsi)        # Store original stack pointer, to pass to to_call
    mov     %rdi, %rsp          # Switch into new stack
        # Restore callee save registers
    mov     (%rsp), %rbx
    mov     8(%rsp), %rbp
    mov     16(%rsp), %r12
    mov     24(%rsp), %r13
    mov     32(%rsp), %r14
    mov     40(%rsp), %r15
    ldmxcsr 48(%rsp)
    fldcw   52(%rsp)
    add     $64, %rsp           # Restore stack
    ret                         # Return into new coroutine
"}