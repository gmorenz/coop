use super::*;

pub struct Generator<T> {
    inner: Box<GeneratorInner<T>>,
    #[allow(unused)] // Just here to be dropped at the right time.
    stack_data: Stack
}

impl <T> Generator<T> {
    pub fn new<F>(f: F) -> Generator<T>
    where F: FnOnce(&mut FnMut(T) -> ()) -> () + 'static {
        let mut stack_data = Stack::new(32 * 1024 - 8);
        let inner = GeneratorInner::new(f, &mut stack_data);
        Generator{inner, stack_data}
    }
}

impl <T> Iterator for Generator<T> {
    type Item=T;
    fn next(&mut self) -> Option<T> {
        if self.inner.stack.is_some() {
            primitives::switch(
                unsafe{ std::ptr::read(self.inner.stack.as_ref().unwrap()) },
                self.inner.stack.as_mut().unwrap());

            self.inner.holder.take()
        }
        else {
            None
        }
    }
}

struct GeneratorInner<T> {
    holder: Option<T>,
    stack: Option<StackPtr>
}

impl <T> GeneratorInner<T> {
    fn new<F>(f: F, stack: &mut Stack) -> Box<GeneratorInner<T>>
    where F: FnOnce(&mut FnMut(T) -> ()) -> () + 'static {
        // Use null just to get ptr to stack.
        let gen = Box::into_raw(Box::new(GeneratorInner::<T> {
            holder: None,
            stack: Some(NULL_STACK_PTR)
        }));

        primitives::start(stack, move |_stack, parent| {
            unsafe {
                let stack_ptr = (*gen).stack.as_mut().unwrap();
                primitives::switch(parent, stack_ptr);
            }

            f(&mut |yielded_val| {
                unsafe {
                    (*gen).holder = Some(yielded_val);
                    let stack = std::ptr::read((*gen).stack.as_ref().unwrap());
                    let stack_ptr = (*gen).stack.as_mut().unwrap();
                    primitives::switch(stack, stack_ptr);
                }
            });

            let parent = unsafe{ (*gen).stack.take().unwrap() };
            primitives::exit_to(parent);
        });

        unsafe{ Box::from_raw(gen) }
    }
}