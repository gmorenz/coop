use std::{mem, panic, ptr};
use std::cell::{Cell, RefCell, UnsafeCell};
use primitives::{StackPtr, NULL_STACK_PTR, self};
use mio;

use super::Stack;

thread_local! {
    // Global state
    static WAITING_THREADS: RefCell<Vec<StackPtr>> = RefCell::new(vec!());
    static INACTIVE_STACKS: RefCell<Vec<Stack>> = RefCell::new(vec!());

    static MIO_POLL: RefCell<mio::Poll> = RefCell::new(mio::Poll::new().unwrap());
    static MIO_EVENTS: RefCell<mio::Events> = RefCell::new(mio::Events::with_capacity(1024));
    static MIO_ACTIVE: Cell<usize> = Cell::new(0);
    static MIO_TOKEN_MAP: RefCell<Vec<StackPtr>> = RefCell::new(vec!());
    static MIO_FREE_TOKEN: RefCell<Vec<usize>> = RefCell::new(vec!());

    static YIELD_THREAD: UnsafeCell<StackPtr> = UnsafeCell::new(make_yield_thread());
}

fn new_waiting() -> *mut StackPtr {
    WAITING_THREADS.with(|threads| {
        let mut threads = threads.borrow_mut();
        threads.push(NULL_STACK_PTR);
        let index = threads.len() - 1;
        &mut threads[index] as *mut StackPtr
    })
}

fn get_stack(event: mio::Event) -> StackPtr {
    MIO_TOKEN_MAP.with(|map| {
        let mut map = map.borrow_mut();
        let thread = mem::replace(&mut map[event.token().0], NULL_STACK_PTR);
        MIO_FREE_TOKEN.with(|free| {
            free.borrow_mut().push(event.token().0);
        });

        thread
    })
}

fn make_yield_thread() -> StackPtr {
    #[thread_local]
    static mut RET: StackPtr = NULL_STACK_PTR;

    let stack = Stack::new(16 * 1024 - 8);
    primitives::start(stack, move |_stack, parent| {
        unsafe{ primitives::switch(parent, &mut RET) };

        WAITING_THREADS.with( |threads|
            loop {
                let maybe_thread = threads.borrow_mut().pop();
                match maybe_thread {
                    Some(thread) => {
                        let ptr = YIELD_THREAD.with(|it| it.get());
                        unsafe{ primitives::unsafe_switch(thread, ptr) };
                    },
                    None => {
                        MIO_EVENTS.with(|events| {
                            MIO_POLL.with(|poll|
                                poll.borrow().poll(&mut *events.borrow_mut(), None).expect("mio poll failed")
                            );

                            for event in events.borrow().iter() {
                                let thread = get_stack(event);
                                MIO_ACTIVE.with(|active| active.set(active.get() - 1));

                                let ptr = YIELD_THREAD.with(|it| it.get());
                                unsafe{ primitives::unsafe_switch(thread, ptr) };
                            }
                        });
                    }
                }
            }
        )
    });
    unsafe{ return ptr::read(&RET) };
}

pub fn start<F: FnOnce() -> () + 'static + panic::UnwindSafe >(f: F) {
    let mut maybe_stack = None;
    INACTIVE_STACKS.with(|stacks| maybe_stack = stacks.borrow_mut().pop());
    let stack = maybe_stack.unwrap_or_else(|| Stack::new(32 * 1024 - 8));

    primitives::start(stack, move |stack, parent| {
        WAITING_THREADS.with(|threads| {
            threads.borrow_mut().push(parent)
        });

        let res = panic::catch_unwind(f);

        if let Err(_) = res {
            eprintln!("Coroutine panicked, killing");
        }

        // Add stack to queue of stacks to be reused.
        INACTIVE_STACKS.with(|stacks| {
            stacks.borrow_mut().push(stack);
        });

        unsafe {
            let thread = YIELD_THREAD.with(|it| ptr::read(it.get()));
            primitives::exit_to(thread);
        }
    });
}

pub fn await<E: mio::Evented>(handle: &E, interest: mio::Ready) {
    MIO_POLL.with(|poll| {
        MIO_TOKEN_MAP.with(|map| {
            let maybe_token = MIO_FREE_TOKEN.with(|free|
                free.borrow_mut().pop()
            );

            let token = maybe_token.unwrap_or_else(|| {
                let mut map = map.borrow_mut();
                map.push(NULL_STACK_PTR);
                map.len() - 1
            });

            {
                // TODO: This is ugly.
                let poll = poll.borrow();
                let res = poll.register(handle, mio::Token(token), interest,
                    mio::PollOpt::oneshot());
                if !res.is_ok() {
                    poll.reregister(handle, mio::Token(token), interest,
                        mio::PollOpt::oneshot()).expect("Failed to register");
                }
            }
            MIO_ACTIVE.with(|active| active.set(active.get() + 1));

            unsafe {
                let thread = YIELD_THREAD.with(|it| ptr::read(it.get()));
                let ptr = {
                    // Need scope to force drop of map!
                    let mut mbm = map.borrow_mut();
                    let ptr = &mut mbm[token] as *mut StackPtr;
                    ptr
                };
                primitives::unsafe_switch(thread, ptr);
            }
        })
    });
}

/// Runs until all coroutines are definitely finished.
pub fn run() {
    let mut finished = false;
    loop {
        WAITING_THREADS.with(|threads|
            MIO_ACTIVE.with(|active|
                finished = active.get() == 0 && threads.borrow().is_empty()
            )
        );
        if finished { break; }

        unsafe {
            let thread = YIELD_THREAD.with(|it| ptr::read(it.get()));
            primitives::unsafe_switch(thread, new_waiting());
        }
    }
}