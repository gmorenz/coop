#![feature(alloc, heap_api, oom, thread_local, never_type, global_asm)]

extern crate alloc;
extern crate mio;

use primitives::{StackPtr, NULL_STACK_PTR};

pub mod primitives;
pub mod generator;
pub mod coroutine;

struct Stack {
    data: *mut u8,
    size: usize
}

impl Stack {
    fn new(size: usize) -> Stack {
        use alloc::heap::allocate;
        assert_eq!((size + 8) % 16, 0);
        let data = unsafe{ allocate(size, 16 ) };
        if data.is_null() {
            alloc::oom::oom();
        }
        Stack{ data, size }
    }
}

impl Drop for Stack {
    fn drop(&mut self) {
        unsafe{
            alloc::heap::deallocate(self.data, self.size, 16);
        }
    }
}

unsafe impl primitives::StackData for Stack {
    fn start_ptr(&mut self) -> *mut u8 {
        unsafe{ self.data.offset(self.size as isize) }
    }
}