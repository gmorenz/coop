extern crate coop;
extern crate mio;

use coop::coroutine as co;
use mio::tcp::{TcpListener, TcpStream};
use std::net::SocketAddr;
use std::io::{Read, Write};

fn echo(mut stream: TcpStream, _addr: SocketAddr) {
    let mut buf = [0u8; 2048];
    let mut used = 0;
    loop {
        if used == 0 {
            co::await(&stream, mio::Ready::readable());
            used = stream.read(&mut buf[used..]).expect("Failed to read from stream");
            if used == 0 {
                break;
            }
        }
        else {
            co::await(&stream, mio::Ready::writable());
            let res = stream.write(&buf[..used]).expect("Failed to write to stream");
            // Manual memmove, don't know why this isn't in std.
            for i in 0.. used - res {
                buf[i] = buf[res + i];
            }
            used -= res;
        }
    }
}

fn main() {
    let addr = "127.0.0.1:13266".parse().unwrap();
    let server = TcpListener::bind(&addr).unwrap();
    loop {
        co::await(&server, mio::Ready::readable());
        match server.accept() {
            Ok((stream, addr)) => {
                co::start(move || echo(stream, addr));
            }
            Err(e) => {
                println!("Error accepting: {:?}", e);
            }
        }
    }
}