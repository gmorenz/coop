extern crate coop as co;

use co::generator::Generator;

fn range(mut low: usize, high: usize, step: usize) -> Generator<usize> {
    Generator::new(move |yield_val| {
        while low < high {
            yield_val(low);
            low += step;
        }
    })
}

fn main() {
    let mut fib = Generator::new(|yield_val| {
        let mut current = 0;
        let mut next = 1;
        loop {
            yield_val(current);
            next = next + current;
            current = next - current;
        }
    });

    println!("Printing out the fib sequence");
    for x in &mut fib {
        println!("{}", x);
        if x > 500 {
            break;
        }
    }

    println!("Taking a break and printing out a range");
    for x in range(0, 20, 2) {
        println!("{}", x);
    }

    println!("Back to the fib sequence, from where we left off");
    for x in fib {
        if x > 10_000 {
            break;
        }
        println!("{}", x);
    }

    println!("Nested generators just work");
    let outer: Generator<usize> = Generator::new(|yield_val| {
        let mut start: usize = 0;
        loop {
            // For some reason rust can't figure out the type returned by sum
            yield_val(range(start, start * 10, start).sum::<usize>());
            start += 1;
        }
    });

    for x in outer.take(10) {
        println!("{}", x);
    }
}