extern crate coop;

use coop::coroutine as co;

fn main() {
    println!("1");
    co::start(|| {
        co::start(|| println!("2"));
        println!("3")
    });
    println!("4");
}