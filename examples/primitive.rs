#![feature(repr_align, attr_literals)]

extern crate coop;
use coop::primitives::*;

#[repr(align(16))]
struct StackStack([u8; 64 * 1024]);

unsafe impl <'a> StackData for &'a mut StackStack {
    fn start_ptr(&mut self) -> *mut u8 {
        unsafe{ (*self as *mut _ as *mut u8).offset(64 * 1024 - 8) }
    }
}

extern "C" fn print(ret: StackPtr, _args: *mut ()) -> ! {
    println!("5");
    unsafe{ asm::exit_to(ret) };
}

static mut PING_PONG: StackPtr = NULL_STACK_PTR;


extern "C" fn co_count(ret: StackPtr, args: *mut ()) -> ! {
    unsafe {
        println!("{}", args as usize);
        asm::switch(ret, &mut PING_PONG);
        println!("1");
        asm::exit_to(std::ptr::read(&PING_PONG));
    }
}

fn main() {
    let mut stack: StackStack = StackStack([0; 64 * 1024]);

    // Non-ASM
    println!("Hello world");
    start(&mut stack, |_stack, parent| exit_to(parent));
    println!("Whoops, false start");
    println!("Ping");
    start(&mut stack, |_stack, parent| {
        println!("Pong");
        switch(parent, unsafe{ &mut PING_PONG} );
        println!("Polo");
        exit_to(unsafe{ std::ptr::read(&PING_PONG) });
    });
    println!("Marco");
    unsafe{ switch(std::ptr::read(&PING_PONG), &mut PING_PONG) };

    // ASM

    unsafe {
        asm::start((&mut stack).start_ptr(), print, std::ptr::null_mut());
        println!("4");
        asm::start((&mut stack).start_ptr(), co_count, 3usize as *mut ());
        println!("2");
        asm::switch(std::ptr::read(&PING_PONG), &mut PING_PONG);
        println!("Liftoff");
    }

        /*
        start(stack.0.as_mut_ptr(), 64 * 1024, print);
        println!("Ping");
        start(stack.0.as_mut_ptr(), 64 * 1024, ping_pong);
        println!("Marco");
        switch(std::ptr::read(&PING_PONG), &mut PING_PONG);
        */
}